# Ionic Preview App

This app accompanies the Ionic component documentation.

[[LIVE DEMO]](http://ionicframework.com/docs/components/)

### Installation & Development

1. clone this repo: `git clone https://github.com/ionic-team/ionic-preview-app.git`
2. `cd ionic-preview-app`
3. `npm install`
4. run `npm run ionic:serve` from a terminal

### TODO

* make it works with node >= 10 (actually only work in node 8)
* migrate from ionic 4 to 5
* migrate from angular 5 to 9
* has been forced since we will reach to angular@8 but for now we should step from 1 major angular version to another. 
`vp1289511% ./node_modules/.bin/ng update @ionic-native/core@5 @ionic-native/splash-screen@5 @ionic-native/status-bar@5 @angular/core@6
                  Package "@ionic/angular" has an incompatible peer dependency to "@angular/core" (requires ">=8.2.7", would install "6.1.10").
                  Package "@ionic/angular" has an incompatible peer dependency to "@angular/forms" (requires ">=8.2.7", would install "6.1.10").
Incompatible peer dependencies found. See above.
`
* 